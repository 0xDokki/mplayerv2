package theultimatehose.mplayerv2.adapter.songlist;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import theultimatehose.mplayerv2.FrameManager;
import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.song.PlaylistManager;
import theultimatehose.mplayerv2.song.SongManager;

public class PlaylistOverviewAdapter extends RecyclerView.Adapter<SongViewHolder> {

    private Activity context;
    private static List<PlaylistManager.Playlist> cachedList;

    public PlaylistOverviewAdapter(Activity context) {
        this.context = context;
        cachedList = PlaylistManager.playlists;
        Collections.sort(cachedList);
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.frame_list_item_song, parent, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        PlaylistManager.Playlist p = cachedList.get(position);
        holder.txtTitle.setText(p.name);
        holder.txtArtist.setText(String.format(context.getResources().getQuantityText(R.plurals.word_song, p.songs.size()).toString(), p.songs.size()));

        if (p.songs.size() > 0)
            holder.setAsyncImageFromSong(SongManager.songList.get(p.songs.get(0)));
        else holder.setGenericImageDisplay();

        holder.itemView.setOnClickListener(v -> {
            PlaylistAdapter p_adapter = new PlaylistAdapter(PlayerActivity.instance, position);
            FrameManager.setFrameAdapter(p_adapter, true);
        });
    }

    @Override
    public void onViewRecycled(SongViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.imgAlbum.getDrawable() instanceof BitmapDrawable)
            ((BitmapDrawable) holder.imgAlbum.getDrawable()).getBitmap().recycle();
        holder.setGenericImageDisplay();
    }

    @Override
    public int getItemCount() {
        return cachedList.size();
    }

}
