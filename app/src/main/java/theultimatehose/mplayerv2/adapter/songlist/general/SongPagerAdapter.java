package theultimatehose.mplayerv2.adapter.songlist.general;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;

public class SongPagerAdapter extends FragmentPagerAdapter {

    public SongPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new ChildFragment();
        Bundle args = new Bundle();
        args.putInt("pos", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return PlayerActivity.instance.getString(R.string.pager_all_songs);
            case 1:
                return PlayerActivity.instance.getString(R.string.pager_albums);
            case 2:
                return PlayerActivity.instance.getString(R.string.pager_artists);
        }
        return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return 3;
    }

    public static class ChildFragment extends Fragment {
        @Override
        public void onResume() {
            RecyclerView recycler = getView().findViewById(R.id.frame_list_recycler);
            recycler.setHasFixedSize(true);
            recycler.setLayoutManager(new LinearLayoutManager(PlayerActivity.instance));
            getView().setPadding(0, PlayerActivity.instance.findViewById(R.id.tab_bar_main).getHeight(), 0 ,0);

            switch (getArguments().getInt("pos")) {
                case 0:
                    recycler.setAdapter(new AllSongsAdapter());
                    break;
                case 1:
                    recycler.setAdapter(new AlbumAdapter());
                    break;
                case 2:
                    recycler.setAdapter(new ArtistAdapter());
                    break;
            }
            super.onResume();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.frame_list, container, false);
        }
    }
}
