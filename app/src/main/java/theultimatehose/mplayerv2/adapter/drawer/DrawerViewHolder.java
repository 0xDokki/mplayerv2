package theultimatehose.mplayerv2.adapter.drawer;

import android.app.Activity;
import android.support.annotation.LayoutRes;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import theultimatehose.mplayerv2.FrameManager;
import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.adapter.GenericRecyclerAdapter;
import theultimatehose.mplayerv2.adapter.GenericViewHolder;
import theultimatehose.mplayerv2.util.UnitConverter;

public class DrawerViewHolder extends GenericViewHolder {

    private TextView text;
    private ImageView img;

    private static View[] backgrounds;

    public DrawerViewHolder(View itemView, int viewType, GenericRecyclerAdapter parent) {
        super(itemView, viewType, parent);
        text = itemView.findViewById(R.id.drawer_item_text);
        img = itemView.findViewById(R.id.drawer_item_image);

        itemView.setOnClickListener(new ClickListener());

        if (backgrounds == null)
            backgrounds = new View[parent.getItemCount()];
    }

    @Override
    public void bind(int position, Object[] args) {

        switch (viewType) {
            case 1:
                img.setImageResource((Integer) args[0]);
                text.setText((Integer) args[1]);
                itemView.setTag(position);

                backgrounds[position] = itemView.findViewById(R.id.drawer_item_background);
                break;
        }

    }

    private class ClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if (v.getTag() == null) return;

            int id = (Integer) v.getTag();
            @LayoutRes int content;

            switch (id) {
                case 1:
                    content = R.layout.frame_player;
                    break;
                case 5:
                    content = R.layout.frame_pager;
                    break;
                default:
                    content = R.layout.frame_list;
                    break;
            }
            FrameLayout layout = ((Activity)parent.context).findViewById(R.id.player_content);
            View view = LayoutInflater.from(parent.context).inflate(content, layout, false);
            layout.removeAllViews();
            layout.addView(view);

            FrameManager.initFrame(id);

            for (View bg : backgrounds) {
                if (bg != null) {
                    bg.setBackgroundResource(R.color.Red_700);
                    bg.setElevation(UnitConverter.convertDpToPixel(5));
                }
            }
            View item = v.findViewById(R.id.drawer_item_background);
            item.setBackgroundResource(R.color.Red_900);
            item.setElevation(UnitConverter.convertDpToPixel(7));

            DrawerLayout drawer = ((Activity)parent.context).findViewById(R.id.player_drawer);
            drawer.closeDrawers();
        }
    }

}
