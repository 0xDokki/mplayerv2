package theultimatehose.mplayerv2;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import theultimatehose.mplayerv2.adapter.songlist.PlaylistAdapter;
import theultimatehose.mplayerv2.adapter.songlist.PlaylistOverviewAdapter;
import theultimatehose.mplayerv2.adapter.songlist.SongQueueAdapter;
import theultimatehose.mplayerv2.adapter.songlist.general.SongPagerAdapter;
import theultimatehose.mplayerv2.util.FragmentUtil;

public class FrameManager {

    private static int currentFrame;

    public static void initFrame(int id) {
        currentFrame = id;
        if (id == 1) {
            PlayerActivity.instance.findViewById(R.id.tab_bar_main).setVisibility(View.GONE);
        } else {

            if (id == 2) {
                SongQueueAdapter q_adapter = new SongQueueAdapter(PlayerActivity.instance);
                setFrameAdapter(q_adapter, false);
            } else if (id == 3) {
                PlaylistAdapter pl_adapter = new PlaylistAdapter(PlayerActivity.instance);
                setFrameAdapter(pl_adapter, false);
            } else if (id == 4) {
                PlaylistOverviewAdapter pl_adapter = new PlaylistOverviewAdapter(PlayerActivity.instance);
                setFrameAdapter(pl_adapter, false);
            } else if (id == 5) {
                SongPagerAdapter sp_adapter = new SongPagerAdapter(PlayerActivity.instance.getSupportFragmentManager());
                FragmentUtil.removeAllFragments(PlayerActivity.instance.getSupportFragmentManager());
                ViewPager pager = PlayerActivity.instance.findViewById(R.id.frame_pager);
                pager.setAdapter(sp_adapter);

                TabLayout tabber = PlayerActivity.instance.findViewById(R.id.tab_bar_main);
                tabber.setupWithViewPager(pager);
                tabber.setVisibility(View.VISIBLE);
            }
        }
    }

    public static void setFrameAdapter(RecyclerView.Adapter adapter, boolean temp) {
        if (currentFrame != 1) {
            RecyclerView frame = PlayerActivity.instance.findViewById(R.id.frame_list_recycler);
            frame.setHasFixedSize(true);
            frame.setLayoutManager(new LinearLayoutManager(PlayerActivity.instance));
            RecyclerView.Adapter prevAdapter = frame.getAdapter();
            frame.setAdapter(adapter);

            PlayerActivity.instance.findViewById(R.id.tab_bar_main).setVisibility(View.GONE);

            if (temp)
                PlayerActivity.instance.backActionStack.push(() -> frame.setAdapter(prevAdapter));
        }
    }

}
