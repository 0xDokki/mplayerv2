package theultimatehose.mplayerv2.song;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;

public class SongManager {

    public static List<Song> songList = new ArrayList<>();

    private static FileFilter filter = pathname -> pathname.isDirectory() || pathname.getName().toLowerCase().endsWith(".mp3");

    public static void refreshList(Activity context) {
        final ProgressDialog dialog = ProgressDialog.show(context, "", context.getString(R.string.dialog_searching_for_songs), false, false);
        dialog.show();

        boolean b = loadSongList();
        if (b) dialog.dismiss();

        new Thread(() -> {
            List<Song> tmpList = new ArrayList<>();
            File[] storage = new File("/storage/").listFiles();
            for (File f : storage) if (f.isDirectory() && f.canRead()) searchDir(f, tmpList);

            songList = tmpList;
            saveSongList();
            dialog.dismiss();
        }).start();
    }

    public static List<Integer> getIntList() {
        List<Integer> lst = new ArrayList<>(songList.size());
        for (int i = 0; i < songList.size(); i++) lst.add(i);
        return lst;
    }

    private static void searchDir(File parent, List<Song> target) {
        for (File f : parent.listFiles(filter)) {
            if (f.isDirectory()) {
                searchDir(f, target);
            } else {
                if (f.getName().toLowerCase().endsWith(".mp3")) {
                    Song s = new Song(f.getAbsolutePath(), false);
                    target.add(s);
                }
            }
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void saveSongList() {
        File cache = new File(PlayerActivity.instance.getFilesDir(), "songlist.json");
        cache.mkdirs();
        cache.delete();

        JSONArray array = new JSONArray();
        for (Song s : songList) array.put(s.path);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(cache));
            bw.write(array.toString());
            bw.flush();
            bw.close();
        } catch (Exception e) {
            Log.e("MPlayer v2", "Error saving song list.", e);
        }
    }

    private static boolean loadSongList() {
        File cache = new File(PlayerActivity.instance.getFilesDir(), "songlist.json");
        if (!cache.exists()) return false;

        try {
            BufferedReader br = new BufferedReader(new FileReader(cache));
            JSONArray array = new JSONArray(br.readLine());

            for (int i = 0; i < array.length(); i++) {
                String path = array.getString(i);
                Song s = new Song(path, false);
                songList.add(s);
            }

            return true;
        } catch (Exception e) {
            Log.e("MPlayer v2", "Error loading song list", e);
        }

        return false;
    }

}
