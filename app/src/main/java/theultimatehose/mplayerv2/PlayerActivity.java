package theultimatehose.mplayerv2;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.Stack;

import theultimatehose.mplayerv2.adapter.GenericRecyclerAdapter;
import theultimatehose.mplayerv2.adapter.drawer.DrawerViewHolder;
import theultimatehose.mplayerv2.song.Player;
import theultimatehose.mplayerv2.song.PlaylistManager;
import theultimatehose.mplayerv2.song.SongManager;
import theultimatehose.mplayerv2.util.Action;
import theultimatehose.mplayerv2.util.UnitConverter;

public class PlayerActivity extends AppCompatActivity {

    public boolean isFooterVisible;
    public boolean isFabPresent = true;

    public Stack<Action> backActionStack = new Stack<>();

    public static PlayerActivity instance;

    private static final int REQUEST_READ_EXTERNAL_STORAGE = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        instance = this;

        Toolbar tb = findViewById(R.id.player_toolbar);
        setSupportActionBar(tb);

        RecyclerView mRecyclerView = findViewById(R.id.drawer_recycler);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        GenericRecyclerAdapter<DrawerViewHolder> adapter = new GenericRecyclerAdapter<>(this, DrawerViewHolder.class);
        adapter.addViewType(0, R.layout.drawer_header);
        adapter.addViewType(1, R.layout.drawer_item);

        adapter.addViewChild(0, 0);
        adapter.addViewChild(1, 1, R.drawable.ic_play, R.string.drawer_item_player);
        adapter.addViewChild(2, 1, R.drawable.ic_queue, R.string.drawer_item_queue);
        adapter.addViewChild(3, 1, R.drawable.ic_favourite, R.string.drawer_item_favourites);
        adapter.addViewChild(4, 1, R.drawable.ic_list, R.string.drawer_item_playlist);
        adapter.addViewChild(5, 1, R.drawable.ic_album, R.string.drawer_item_songs);

        mRecyclerView.setAdapter(adapter);

        final FloatingActionButton fab = findViewById(R.id.player_fab);
        if (isFabPresent) {
            fab.setOnClickListener(view -> {
                if (!isFooterVisible) {
                    final LinearLayout footer = findViewById(R.id.footer_include);

                    Animator anim = ViewAnimationUtils.createCircularReveal(footer,
                            (int) (footer.getWidth() - (fab.getWidth() / 2) - UnitConverter.convertDpToPixel(10)),
                            (int) (footer.getHeight() - (fab.getHeight() / 2) - UnitConverter.convertDpToPixel(10)),
                            0,
                            (float) Math.hypot(footer.getWidth(), footer.getHeight()));

                    anim.setDuration(400);
                    anim.setInterpolator(new AccelerateDecelerateInterpolator());
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            footer.setEnabled(true);
                            fab.setVisibility(View.GONE);
                            isFooterVisible = true;
                        }
                    });
                    anim.start();
                    footer.setVisibility(View.VISIBLE);
                }
            });

        }

        FrameLayout layout = findViewById(R.id.player_content);
        layout.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
            @Override
            public void onChildViewAdded(View parent, View child) {
                if (child.getTag() instanceof String && child.getTag().equals(getString(R.string.tag_no_fab))) {
                    fab.setVisibility(View.GONE);
                    findViewById(R.id.footer_include).setVisibility(View.GONE);
                    isFabPresent = false;
                } else {
                    fab.setVisibility(View.VISIBLE);
                    isFabPresent = true;
                }
            }

            @Override
            public void onChildViewRemoved(View parent, View child) {}
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            SongManager.refreshList(this);
            PlaylistManager.loadPlaylists();
        }

        Player.setupUiUpdater(this);
        Player.startService();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SongManager.refreshList(this);
                    PlaylistManager.loadPlaylists();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_player, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_playlist_add:
                AlertDialog.Builder ad = new AlertDialog.Builder(this, R.style.AppTheme_Dialog);

                EditText txt = new EditText(this);
                txt.setTextColor(getResources().getColor(R.color.Black));
                txt.setHint(R.string.word_name);
                ad.setView(txt);

                ad.setTitle("Create Playlist");
                ad.setNegativeButton(R.string.dialog_option_cancel, null);
                ad.setPositiveButton(R.string.dialog_option_create, (dialog, which) -> PlaylistManager.addPlaylist(txt.getText().toString()));
                ad.create().show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isFooterVisible && isFabPresent) {
            final FloatingActionButton fab = findViewById(R.id.player_fab);
            final LinearLayout footer = findViewById(R.id.footer_include);

            Animator anim = ViewAnimationUtils.createCircularReveal(footer,
                    (int) (footer.getWidth() - (fab.getWidth() / 2) - UnitConverter.convertDpToPixel(10)),
                    (int) (footer.getHeight() - (fab.getHeight() / 2) - UnitConverter.convertDpToPixel(10)),
                    (float) Math.hypot(footer.getWidth(), footer.getHeight()),
                    0);

            anim.setDuration(400);
            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    footer.setVisibility(View.GONE);
                    isFooterVisible = false;
                }
            });
            anim.start();
            fab.setVisibility(View.VISIBLE);
        } else if (!backActionStack.isEmpty()) {
            backActionStack.pop().perform();
        } else
            super.onBackPressed();
    }
}
