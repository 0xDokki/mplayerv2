package theultimatehose.mplayerv2.util;

public interface Action {
    void perform();
}
