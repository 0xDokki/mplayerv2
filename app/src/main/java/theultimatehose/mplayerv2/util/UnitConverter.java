package theultimatehose.mplayerv2.util;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class UnitConverter {

    public static float convertDpToPixel(float dp){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static float convertPixelsToDp(float px){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static String msToTimeString(int msec) {
        int hour, min, sec;

        sec = (msec % (1000 * 60 * 60)) % (1000 * 60) / 1000;
        min = (msec % (1000 * 60 * 60)) / (1000 * 60);
        hour = msec / (1000 * 60 * 60);

        String total = "";

        if (hour != 0)
            total += hour + ":";

        if (min < 10)
            total += 0;
        total += min + ":";

        if (sec < 10)
            total += 0;
        total += sec;

        return total;
    }

}
